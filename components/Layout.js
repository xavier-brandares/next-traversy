import Nav from './Nav';
import MyMeta from './MyMeta';
import styles from '../styles/Layout.module.css';
import { Header } from './Header';

export const Layout = ({ children }) => {
	return (
		<>
			<MyMeta />
			<Nav />
			<div className={styles.container}>
				<main className={styles.main}>
					<Header />
					{children}
				</main>
			</div>
		</>
	);
};
