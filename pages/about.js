import MyMeta from '../components/MyMeta';
import styles from '../styles/Layout.module.css';

const about = () => {
	return (
		<div className={styles.container}>
			<MyMeta title="About" />
			<div>About</div>
		</div>
	);
};

export default about;
